package main

import (
	"fmt"
	"net/http"
	"context"
	"net/url"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	_ "github.com/go-sql-driver/mysql"
	"bitbucket.org/mejianiko/ni_me/backend/db"
	"bitbucket.org/mejianiko/ni_me/backend/response"
	"bitbucket.org/mejianiko/ni_me/backend/utils"
)

func handler(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	data, err := url.ParseQuery(request.Body)
	if err != nil {
		return response.Failed("Failed to parse form data from request", http.StatusBadRequest), nil
	}
	if ok := utils.ContainsAllKeys(data, "username", "password"); !ok {
		return response.Failed("Expecting username and password to be present", http.StatusBadRequest), nil
	}
	accountID, err := db.AuthenticateAccount(data.Get("username"), data.Get("password"))
	if err != nil {
		return response.Failed(fmt.Sprintf("Authentication failed: %v", err), http.StatusUnauthorized), nil
	}
	return response.Success(map[string]interface{} {
		"id": accountID,
		"success": true,
	}, http.StatusOK), nil
}

func main() {
	lambda.Start(handler)
}