package db

import (
	"bitbucket.org/mejianiko/ni_me/backend/utils"
	"fmt"
	"bitbucket.org/mejianiko/ni_me/backend/profile"
	"database/sql"
)

const stmtAddProfile = "INSERT INTO profiles (id, email) VALUES (?, ?)"

// InsertNewProfile adds a record in profiles table
func InsertNewProfile(accountID, email string) error {
	db, err := sql.Open("mysql", dataSource)
	if err != nil {
		return err
	}
	_, err = db.Exec(stmtAddProfile, accountID, email)
	return err
}

const stmtEditProfile = "UPDATE profiles SET fullname=?, address=?, email=?, telephone=? WHERE id=?"

// UpdateProfile updates all fields/columns in profiles table
func UpdateProfile(p *profile.Profile, id string) error {
	db, err := sql.Open("mysql", dataSource)
	if err != nil {
		return err
	}
	_, err = db.Exec(stmtEditProfile, p.Fullname, p.Address, p.Email, p.Telephone, id)
	return cascadeUpdateEmail(id, p.Email)
}

const stmtGetProfile = "SELECT fullname, address, email, telephone FROM profiles WHERE id=?"

func GetProfile(id string) (*profile.Profile, error) {
	db, err := sql.Open("mysql", dataSource)
	if err != nil {
		return nil, err
	}
	rows, err := db.Query(stmtGetProfile, id)
	if err != nil {
		return nil, err
	}
	if !rows.Next() {
		return nil, fmt.Errorf("Profile with id (%s) does not exists", id)
	}
	defer rows.Close()
	p := &profile.Profile{ID: id}
	var fullname, address, tel *string
	err = rows.Scan(&fullname, &address, &p.Email, &tel)
	if err != nil {
		return nil, err
	}
	p.Fullname = utils.GetString(fullname)
	p.Address = utils.GetString(address)
	p.Telephone = utils.GetString(tel)
	return p, nil
}