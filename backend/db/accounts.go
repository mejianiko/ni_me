package db

import (
	"database/sql"

	"github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
)

const stmtAddRecord = "INSERT INTO accounts VALUES (?, ?, ?)"

// InsertNewAccount adds a new record in accounts table and returns the id
func InsertNewAccount(username, password string) (string, error) {
	db, err := sql.Open("mysql", dataSource)
	if err != nil {
		return "", err
	}
	id := uuid.NewV4()
	pw, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	_, err = db.Exec(stmtAddRecord, id, username, pw)
	if err != nil {
		return "", err
	}
	return id.String(), nil
}

const stmtGetAccountByUsername = "SELECT * FROM accounts WHERE username = ?"

// AccountUsernameAlreadyExists returns true if username already exists in the accounts table
func AccountUsernameAlreadyExists(username string) (bool, error) {
	db, err := sql.Open("mysql", dataSource)
	if err != nil {
		return false, err
	}
	rows, err := db.Query(stmtGetAccountByUsername, username)
	if err != nil {
		return false, err
	}
	defer rows.Close()
	return rows.Next(), nil
}

const stmtGetAccountIDPasswordByUsername = "SELECT id, password FROM accounts WHERE username = ?"

// AuthenticateAccount validates account login credentials
func AuthenticateAccount(username, password string) (string, error) {
	db, err := sql.Open("mysql", dataSource)
	if err != nil {
		return "", err
	}
	rows, err := db.Query(stmtGetAccountIDPasswordByUsername, username)
	if err != nil {
		return "", err
	}
	defer rows.Close()
	if !rows.Next() {
		return "", nil
	}
	var id string
	var hashedpw []byte
	err = rows.Scan(&id, &hashedpw)
	if err != nil {
		return "", err
	}
	err = bcrypt.CompareHashAndPassword(hashedpw, []byte(password))
	return id, err
}

const stmtEditUsername = "UPDATE accounts SET username=? WHERE id=?"

func cascadeUpdateEmail(id, email string) error {
	db, err := sql.Open("mysql", dataSource)
	if err != nil {
		return err
	}
	_, err = db.Exec(stmtEditUsername, email, id)
	return err
}