package db

import (
	"fmt"
	"os"
)

var dataSource = fmt.Sprintf(
	"%s:%s@tcp(%s:%s)/%s", 
	os.Getenv("DB_USER"),
	os.Getenv("DB_USER_PW"),
	os.Getenv("DB_HOST"),
	os.Getenv("DB_PORT"),
	os.Getenv("DB_NAME"),
)