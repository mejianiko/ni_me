package main

import (
	"fmt"
	"bitbucket.org/mejianiko/ni_me/backend/db"
	"bitbucket.org/mejianiko/ni_me/backend/profile"
	"bitbucket.org/mejianiko/ni_me/backend/response"
	"context"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	_ "github.com/go-sql-driver/mysql"
	"net/http"
)

func handler(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	id := request.PathParameters["id"]
	p, err := profile.Parse(request.Body)
	if err != nil {
		return response.Failed("Failed to convert payload into a Profile object", http.StatusBadRequest), nil
	}
	err = db.UpdateProfile(p, id)
	if err != nil {
		return response.Failed(fmt.Sprintf("Failed updating profile, %v", err), http.StatusBadRequest), nil
	}
	return response.Success(nil, http.StatusNoContent), nil
}

func main() {
	lambda.Start(handler)
}
