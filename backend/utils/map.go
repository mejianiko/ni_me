package utils

import (
	"net/url"
)

// ContainsAllKeys checks keys of a map if all given keys are present
func ContainsAllKeys(m url.Values, keys ...string) bool {
	for _, key := range keys {
		if _, ok := m[key]; !ok {
			return false
		} 
	}
	return true
}
