package utils

import (
	"net/url"
	"testing"
)

func TestContainsAllKeys(t *testing.T) {
	cases := []struct {
		input string
		expected bool
	} {
		{ "username=test%40mailinator.com&pasword=test1234", false },
		{ "username=test%40mailinator.com&password=test1234", true },
	}
	for _, test := range cases {
		v, err := url.ParseQuery(test.input)
		if err != nil {
			t.Errorf("%v", err)
		}
		if ok := ContainsAllKeys(v, "username", "password"); ok != test.expected {
			t.Errorf("expected %v, got %v", test.expected, ok)
		}
	}
}
