package utils

// GetString returns the value of scannedItem as string
func GetString(scannedItem *string) string {
	if scannedItem == nil {
		return ""
	}
	return *scannedItem
}