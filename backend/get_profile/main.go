package main

import (
	"net/http"
	"bitbucket.org/mejianiko/ni_me/backend/response"
	"bitbucket.org/mejianiko/ni_me/backend/db"
	"github.com/aws/aws-lambda-go/events"
	"context"
	"github.com/aws/aws-lambda-go/lambda"
	_ "github.com/go-sql-driver/mysql"
)

func handler(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	id := request.PathParameters["id"]
	p, err := db.GetProfile(id)
	if err != nil {
		return response.Failed(err.Error(), http.StatusNotFound), nil
	}
	return response.Success(p, http.StatusOK), nil
}

func main() {
	lambda.Start(handler)
}