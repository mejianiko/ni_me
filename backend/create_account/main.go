package main

import (
	"fmt"
	"context"
	"net/http"
	"net/url"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	_ "github.com/go-sql-driver/mysql"
	"bitbucket.org/mejianiko/ni_me/backend/db"
	"bitbucket.org/mejianiko/ni_me/backend/response"
	"bitbucket.org/mejianiko/ni_me/backend/utils"
)

func handler(ctx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	data, err := url.ParseQuery(request.Body)
	if err != nil {
		return response.Failed("Failed to parse form data from request", http.StatusBadRequest), nil
	}
	if ok := utils.ContainsAllKeys(data, "username", "password"); !ok {
		return response.Failed("Expecting username and password to be present", http.StatusBadRequest), nil
	}
	accountExists, err := db.AccountUsernameAlreadyExists(data.Get("username"))
	if err != nil {
		return response.Failed(err.Error(), http.StatusInternalServerError), nil
	}
	if accountExists {
		return response.Failed(
			fmt.Sprintf("Account with username '%s' already exists", data.Get("username")),
			http.StatusUnprocessableEntity,
		), nil
	}
	accountID, err := db.InsertNewAccount(data.Get("username"), data.Get("password"))
	if err != nil {
		return response.Failed(err.Error(), http.StatusInternalServerError), nil
	}
	err = db.InsertNewProfile(accountID, data.Get("username"))
	if err != nil {
		return response.Failed(err.Error(), http.StatusInternalServerError), nil
	}
	return response.Success(map[string]interface{} {
		"id": accountID,
	}, http.StatusCreated), nil
}

func main() {
	lambda.Start(handler)
}