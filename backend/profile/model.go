package profile

import (
	"encoding/json"
)

// Profile object
type Profile struct {
	ID        string `json:"id"`
	Fullname  string `json:"fullname,omitempty"`
	Address   string `json:"address,omitempty"`
	Email     string `json:"email"`
	Telephone string `json:"tel,omitempty"`
}

// Parse unmarshals data to a Profile object
func Parse(data string) (*Profile, error) {
	var profile = new(Profile)
	if err := json.Unmarshal([]byte(data), profile); err != nil {
		return nil, err
	}
	return profile, nil
}
