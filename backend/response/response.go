package response

import (
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
)

func responseWithJSONData(data interface{}, status int) events.APIGatewayProxyResponse {
	responsePayload, _ := json.Marshal(data)
	return events.APIGatewayProxyResponse{
		StatusCode: status,
		Headers: map[string]string{ "Content-Type": "application/json" },
		Body: string(responsePayload),
	}
}

// Failed response helper
func Failed(msg string, status int) events.APIGatewayProxyResponse {
	return responseWithJSONData(map[string]interface{} {
		"Error": msg,
	}, status)
}

// Success response helper
func Success(payload interface{}, status int) events.APIGatewayProxyResponse {
	if payload != nil {
		return responseWithJSONData(payload, status)
	}
	return events.APIGatewayProxyResponse{StatusCode: status}
}