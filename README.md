# (Golang Practical Test) User Creation and Login Process

## User Database

```
+----------+        +-----------+
| accounts |        | profiles  |
+----------+        +-----------+
| id       |        | id        |
| username |        | fullname  |
| password |        | address   |
+----------+        | email     |
                    | telephone |
                    +-----------+
```

DB Engine: MySQL v5.6.x

Reference to UserDB deployment script is provided. (see [deployment/db.yml](./deployment/db.yml))
