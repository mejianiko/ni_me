package main

import (
	"os"
	"bytes"
	"crypto/rand"
	"crypto/tls"
	"encoding/json"
	"io/ioutil"
	"bitbucket.org/mejianiko/ni_me/frontend/data"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"html/template"
	"net/http"
)

var apiBaseURL = os.Getenv("API_BASE_URL")

var store *sessions.CookieStore

func newSession(w http.ResponseWriter, r *http.Request, id string) error {
	sess, err := store.Get(r, id)
	if err != nil {
		return err
	}
	sess.Values["authenticated"] = true
	sess.Save(r, w)
	return nil
}

func index(w http.ResponseWriter, r *http.Request) {
	page := &struct{ Title string }{"Hi there!"}
	t := template.Must(template.ParseFiles("tmpl/index.html"))
	t.Execute(w, page)
}

func isAuthenticated(r *http.Request, id string) bool {
	sess, _ := store.Get(r, id)
	authenticated, ok := sess.Values["authenticated"].(bool)
	return authenticated && ok
}

func saveProfile(w http.ResponseWriter, r *http.Request, profile *data.Profile) {
	sess, _ := store.Get(r, profile.ID)
	p, _ := json.Marshal(profile)
	sess.Values["profile"] = p
	sess.Save(r, w)
}

func loadProfile(r *http.Request, id string) *data.Profile {
	sess, _ := store.Get(r, id)
	pdata := sess.Values["profile"].([]byte)
	var profile = new(data.Profile)
	json.Unmarshal(pdata, profile)
	return profile
}

func enterSite(w http.ResponseWriter, r *http.Request, path string, successStatus int) error {
	a := data.Account{Username: r.PostFormValue("username"), Password: r.PostFormValue("password")}
	if err := a.Validate(); err != nil {
		return err
	}
	resp, err := http.PostForm(fmt.Sprintf("%s/%s", apiBaseURL, path), r.PostForm)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	var payload interface{}
	if err = json.Unmarshal(body, &payload); err != nil {
		return err
	}
	if resp.StatusCode == successStatus {
		id := payload.(map[string]interface{})["id"].(string)
		if err = newSession(w, r, id); err != nil {
			return err
		}
		http.Redirect(w, r, fmt.Sprintf("/profile/%s", id), http.StatusFound)
		return nil
	}
	errMsg := payload.(map[string]interface{})["Error"].(string)
	http.Error(w, errMsg, resp.StatusCode)
	return nil
}

func signup(w http.ResponseWriter, r *http.Request) {
	if err := enterSite(w, r, "accounts", http.StatusCreated); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func signin(w http.ResponseWriter, r *http.Request) {
	if err := enterSite(w, r, "authenticate", http.StatusOK); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func profile(w http.ResponseWriter, r *http.Request) {
	pathParam := mux.Vars(r)
	if !isAuthenticated(r, pathParam["id"]) {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	resp, err := http.Get(fmt.Sprintf("%s/%s/%s", apiBaseURL, "profiles", pathParam["id"]))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if resp.StatusCode == http.StatusOK {
		var profile data.Profile
		if err = json.Unmarshal(body, &profile); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		saveProfile(w, r, &profile)
		t := template.Must(template.ParseFiles("tmpl/profile.html"))
		t.Execute(w, profile)
		return
	}
	var errorBody interface{}
	if err = json.Unmarshal(body, &errorBody); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	errMsg := errorBody.(map[string]interface{})["Error"].(string)
	http.Error(w, errMsg, resp.StatusCode)
}

func profileEdit(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		profileEditSave(w, r)
		return
	}
	pathParam := mux.Vars(r)
	if !isAuthenticated(r, pathParam["id"]) {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	profile := loadProfile(r, pathParam["id"])
	t := template.Must(template.ParseFiles("tmpl/edit-profile.html"))
	t.Execute(w, profile)
}

func profileEditSave(w http.ResponseWriter, r *http.Request) {
	pathParam := mux.Vars(r)
	profile := data.Profile{
		ID: pathParam["id"],
		Fullname: r.PostFormValue("fullname"),
		Address: r.PostFormValue("address"),
		Email: r.PostFormValue("email"),
		Telephone: r.PostFormValue("tel"),
	}
	data, err := json.Marshal(profile)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	body := bytes.NewReader(data)
	req, err := http.NewRequest(
		http.MethodPut,
		fmt.Sprintf("%s/%s/%s", apiBaseURL, "profiles", profile.ID),
		body,
	)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()
	payload, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if resp.StatusCode == http.StatusNoContent {
		http.Redirect(w, r, "/profile/" + profile.ID, http.StatusFound)
		return
	}
	var errorBody interface{}
	if err = json.Unmarshal(payload, &errorBody); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	errMsg := errorBody.(map[string]interface{})["Error"].(string)
	http.Error(w, errMsg, resp.StatusCode)
}

func signout(w http.ResponseWriter, r *http.Request) {
	pathParam := mux.Vars(r)
	sess, _ := store.Get(r, pathParam["id"])
	sess.Options.MaxAge = -1
	sess.Save(r, w)
	http.Redirect(w, r, "/", http.StatusFound)
}

func init() {
	var key = make([]byte, 32)
	_, err := rand.Read(key)
	if err != nil {
		panic(err)
	}
	store = sessions.NewCookieStore(key)
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", index)
	r.HandleFunc("/signup", signup).Methods("POST")
	r.HandleFunc("/signin", signin).Methods("POST")
	r.HandleFunc("/profile/{id}", profile)
	r.HandleFunc("/profile/{id}/edit", profileEdit).Methods("GET", "POST")
	r.HandleFunc("/signout/{id}", signout)

	cfg := &tls.Config{
		InsecureSkipVerify: true,
	}
	http.DefaultClient.Transport = &http.Transport{
		TLSClientConfig: cfg,
	}
	http.ListenAndServe(":80", r)
}
