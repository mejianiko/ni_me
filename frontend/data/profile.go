package data

// Profile of the user
type Profile struct {
	ID        string `json:"id"`
	Fullname  string `json:"fullname"`
	Address   string `json:"address"`
	Email     string `json:"email"`
	Telephone string `json:"tel"`
}
