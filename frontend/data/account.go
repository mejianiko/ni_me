package data

import (
	"errors"
	"bytes"
	"regexp"
)

// Account of the user
type Account struct {
	Username string
	Password string
}

func (a Account) Validate() error {
	var buffer bytes.Buffer
	if len(a.Username) == 0 {
		buffer.WriteString("Email is required\n")
	} else {
		emailPattern := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
		if !emailPattern.MatchString(a.Username) {
			buffer.WriteString("Invalid email format\n")
		}
	}
	if len(a.Password) == 0 {
		buffer.WriteString("Password is required\n")
	}
	if buffer.Len() > 0 {
		return errors.New(buffer.String())
	}
	return nil
}