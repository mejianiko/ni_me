ENV ?= Dev
AWS_REGION ?= us-west-1
USERDB_STACKNAME = rds-userdb-$(ENV)
DB_MASTER_USER_PASSWORD ?= secretpassword
DB_NAME ?= UsersDB

DB_CLIENT = docker-compose run --rm --entrypoint mysql db -h usersdb.local \
	-u root --password=$(DB_MASTER_USER_PASSWORD) $(DB_NAME)

GOLANG = docker-compose run --rm golang

GOBUILD = GOOS=linux go build -v -ldflags '-d -s -w' -a -tags netgo -installsuffix netgo

SLS = docker-compose run --rm serverless

SAM = docker-compose run --rm --service-ports aws-sam-local sam

# Targets createDB/deleteDB/checkDBStackEvents are used to manage Amazon RDS DB instance

createDB:
	@aws cloudformation deploy --template-file deployment/db.yml \
	--stack-name $(USERDB_STACKNAME) \
	--parameter-overrides MasterUserPassword=$(DB_MASTER_USER_PASSWORD) \
	--region $(AWS_REGION)

deleteDB:
	@aws cloudformation delete-stack --stack-name $(USERDB_STACKNAME) --region $(AWS_REGION)

checkDBStackEvents:
	@aws cloudformation describe-stack-events --stack-name $(USERDB_STACKNAME) --region $(AWS_REGION)

# Database targets

startdb:
	@echo 'MYSQL_ROOT_PASSWORD=$(DB_MASTER_USER_PASSWORD)' > db.env
	@echo 'MYSQL_DATABASE=$(DB_NAME)' >> db.env
	@docker-compose up -d db

stopdb:
	docker-compose stop db

purgedb: stopdb
	@rm -rf ./data

dbmigrate: startdb
	@$(DB_CLIENT) < ./migration/create_tables.sql

dbshell: startdb
	@$(DB_CLIENT)

# Golang builds and dependency management

goshell:
	@$(GOLANG) sh

initdeps:
	@$(GOLANG) dep init

addDep:
	@$(GOLANG) dep ensure -add $(DEP)

syncDeps:
	@$(GOLANG) dep ensure

buildFrontend:
	@docker-compose build --no-cache frontend

frontend:
	@docker-compose up -d frontend

buildBackend:
	@$(GOLANG) make _buildCreateAccount _buildAuthenticate _buildEditProfile _buildGetProfile

buildCreateAccount:
	@$(GOLANG) make _buildCreateAccount

buildAuthenticate:
	@$(GOLANG) make _buildAuthenticate

buildEditProfile:
	@$(GOLANG) make _buildEditProfile

buildGetProfile:
	@$(GOLANG) make _buildGetProfile

_buildCreateAccount:
	@rm -f bin/createAccount bin/createAccount.zip
	@$(GOBUILD) -o bin/createAccount backend/create_account/main.go
	@zip -D bin/createAccount.zip bin/createAccount

_buildAuthenticate:
	@rm -f bin/authenticate bin/authenticate.zip
	@$(GOBUILD) -o bin/authenticate backend/authenticate/main.go
	@zip -D bin/authenticate.zip bin/authenticate

_buildEditProfile:
	@rm -f bin/editProfile bin/editProfile.zip
	@$(GOBUILD) -o bin/editProfile backend/edit_profile/main.go
	@zip -D bin/editProfile.zip bin/editProfile

_buildGetProfile:
	@rm -f bin/getProfile bin/getProfile.zip
	@$(GOBUILD) -o bin/getProfile backend/get_profile/main.go
	@zip -D bin/getProfile.zip bin/getProfile

deployBackend:
	@rm -rf .serverless
	@$(SLS) sls deploy -v

template.yml:
	@$(SLS) sls plugin install --name serverless-sam 
	@$(SLS) sls sam export -o template.yml

local-start-api: template.yml
	@$(SAM) local start-api --docker-volume-basedir=$(PWD) --host 0.0.0.0

shell:
	@$(SLS) sh

.PHONY: frontend
