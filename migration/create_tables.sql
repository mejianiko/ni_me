CREATE TABLE profiles
(
  id CHAR(36) NOT NULL,
  fullname VARCHAR(70),
  address VARCHAR(255),
  email VARCHAR(50) NOT NULL,
  telephone VARCHAR(20),
  PRIMARY KEY (id)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE accounts
(
  id CHAR(36) NOT NULL,
  username VARCHAR(50) NOT NULL,
  password BINARY(60) NOT NULL,
  PRIMARY KEY (id),
  KEY indexusername (username)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
